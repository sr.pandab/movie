package com.flipkart.app.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.flipkart.entity.Brand;

public class BrandRepository {
	public void saveBrandDetails(Brand brand) {
		try {
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sessionFactory = cfg.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(brand);
			transaction.commit();
		} catch (HibernateException e) {
			
		}
	}

}
