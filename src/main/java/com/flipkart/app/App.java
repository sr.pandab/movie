package com.flipkart.app;

import java.util.Date;

import com.flipkart.app.repository.BrandRepository;
import com.flipkart.entity.Brand;

public class App 
{
    public static void main( String[] args )
    {
        Brand brand = new Brand();
        brand.setEstablishDate(new Date());
        brand.setId(1996);
        brand.setName("Reebook");
        brand.setRating("9");
        
        
        BrandRepository brandRepository = new BrandRepository();
        brandRepository.saveBrandDetails(brand);
        System.out.println("inserted successfully");
    }
}
